package com.comcast.cima.test.ui.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.comcast.test.citf.common.ui.pom.SeleniumPageObject;

public class onlineStoreInnerPage extends SeleniumPageObject<onlineStoreInnerPage> {
	
	@FindBy (how = How.XPATH, using = "//p[@class='lead']")
	private WebElement headerMessage;
	
	@FindBy (how = How.XPATH, using = "//a[@class='addToCart']")
	private WebElement addToCart;
	
	@FindBy (how = How.XPATH, using = "//a[@id='open-cart']")
	private WebElement myCart;
	
	@FindBy (how = How.XPATH, using = "//button[@class='close removeProductIcon']")
	private WebElement removeFromCart;
	
	@FindBy (how = How.XPATH, using = "//p[@id='shoppingcarttitle']")
	private WebElement cartInfo;
	
	@FindBy (how = How.XPATH, using = "//div[@id='cartMessage']")
	private WebElement cartMessage;

	@Override
	protected void isLoaded() throws Error {
		Assert.assertTrue(this.isElementPresent(headerMessage));
	}

	@Override
	protected void load() {}
	
	public onlineStoreInnerPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void doAddToCart() {
		if (isElementPresent(this.addToCart)) {
			this.addToCart.click();
		}
	}
	
	public void openCart() {
		if (isElementPresent(this.myCart)) {
			this.myCart.click();
			this.cartInfo.getText();
			this.myCart.click();
			//Assert.assertTrue(this.isElementPresent(this.cartInfo));
		}
	}
	
	public void removeItem() {
		this.myCart.click();
		if (isElementPresent(this.removeFromCart)) {
			this.removeFromCart.click();
		}
	}
	
	public String getCartMessage() {
		String message = null;
		openCart();
		if (isElementPresent(this.cartMessage)) {
			message = this.cartMessage.getText();
		}
		
		return message;
	}
	
	
	private static Logger LOGGER = LoggerFactory.getLogger(onlineStoreInnerPage.class);

}
