#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
Feature: Validate user can add items to the shopping cart

	@WIP
	Scenario Outline: Shopping cart validation
	
		# Request for the store url and user
		Given online store URL and user credentials
		
		# User opens online store and adds the item to shopping cart
		When user opens the online store in "<browser>"
		And user selects to add the book to the shopping cart
		Then user should be able to add the book to the shopping cart	
		
		When user removes the item from cart
		Then the item should be removed from cart
		Examples:
      | browser     |
      | Chrome      |
 	