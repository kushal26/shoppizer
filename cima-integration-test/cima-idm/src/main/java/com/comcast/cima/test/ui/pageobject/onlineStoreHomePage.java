package com.comcast.cima.test.ui.pageobject;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.comcast.test.citf.common.ui.pom.SeleniumPageObject;
import com.comcast.test.citf.core.init.ObjectInitializer;

public class onlineStoreHomePage extends SeleniumPageObject<onlineStoreHomePage> {
	
	private String appURL; 
	
	/*Element details*/
	@FindBy(how = How.XPATH, using = "//ul[@id='mainMenu']/li[2]/a/span[1]")
	private WebElement businessLink;
	
	

	@Override
	protected void isLoaded() throws Error {
		String url = this.driver.getCurrentUrl();
		String path = "";
		try {
			URL u = new URL(url);
			path = u.getPath();
		} catch (MalformedURLException mue) {
			Assert.fail("Unable to constuct URL from value '" + url + "'");
		}
		Assert.assertTrue(path.startsWith("http://") || url.contains("sm-shop"),
				"Current page URL should contain '/sm-shop/', but the actual value is '"
						+ url + "'");
		
	}

	@Override
	protected void load() {
		try	{
			this.driver.get(getAppURL());
			Thread.sleep(5000);
			this.driver.manage().window().maximize();
		} catch (Exception e) {
			LOGGER.error("Error occurred while loading online store home page. ", e);
		}
		
	}
	
	public onlineStoreHomePage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.appURL = null;
	}
	
	
	public Object openBusinessPage() throws Exception{
		if (isElementPresent(businessLink)){
			this.businessLink.click();
			return ObjectInitializer.getPageNavigator().getNextPage(driver, this, this.getPageFlowId());
		}
		return null;
	}
	
	
	public String getAppURL() {
		return appURL;
	}

	public void setAppURL(String appURL) {
		this.appURL = appURL;
	}
	
	
	
	private static Logger LOGGER = LoggerFactory.getLogger(onlineStoreHomePage.class);
	
	

}
